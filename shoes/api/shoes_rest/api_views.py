from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from shoes_rest.models import Shoe, BinsVO
import json


class BinVODetailEncoder(ModelEncoder):
    model = BinsVO
    properties = [
        "closet_name", "bin_number", "bin_size"]


class ListShoesEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer",
                  "model_name",
                  "picture_url",
                  "bins",
                  "color",
                  "id",
                  ]

    def get_extra_data(self, o):
        return {"bins": o.bins.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bins",
        ]
    encoders = {
        "bins": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()

        return JsonResponse(
            {"shoes": shoes},
            encoder=ListShoesEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_number = content["bins"]
            bins = BinsVO.objects.get(bin_number=bin_number)
            content["bins"] = bins
        except BinsVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin number"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )


@require_http_methods (["DELETE", "GET"])
def show_shoe_detail(request, pk):

    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)

        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        shoe = Shoe.objects.get(id=pk).delete()

        return JsonResponse(
            {"message": "deleted"}
        )