from django.urls import path
from shoes_rest.api_views import api_list_shoes, show_shoe_detail


urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk/", show_shoe_detail, name="show_shoe_detail"),
]
