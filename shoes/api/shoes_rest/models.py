from django.db import models
from django.urls import reverse


class BinsVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.closet_name


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(max_length=200, null=True)
    bins = models.ForeignKey(
        BinsVO,
        related_name="bins",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.model_name

    def get_api_url(self):
        return reverse("api_shoe_detail", kwargs={"pk": self.pk})
