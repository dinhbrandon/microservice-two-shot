# Wardrobify

Team:

 Brandon Dinh - Hats
    -Hat properties
        -Fabric
        -Style name
        -Color
        -Picture URL
        -Location in wardrobe

    -Hats Microservice:
        1. Add hats app to Django installed apps (Done)
        2. Create a poller to poll from wardrobe (Done)
        3. Create api urls (api_urls.py) (Done)
        4. Create api views (api_views.py) (Done)
        5. Create models (models.py) (Done)
        hats/poll
        6. Create Hat Form/Hat List components (Done)
        7. Link components to Apps.js via Route (Done)

What I've done is create a file that polls location data from the wardrobe monolith and tied that location data to my Hat model via a value object model which is linked with a Foreign key. The list_hat and show_hat_detail functions were created in my views to handle REST API requests via GET/POST/DELETE which handle the logic that allows hat objects to be created, viewed in detail and deleted. Those view functions are called on inside of my urls, which is tied to the hat microservice on port 8090. My React JS files (HatForm and ListHats) make HTTP API requests to the microservices, where it waits for the JSON data in the form of a promise, which it later parses and manipulates to dynamically display information on the webpage.



Josh Tobin - Shoes
    -Shoes properties
        -Style name
        -Color
        -Picture URL
        -Location in wardrobe

    shoes/api
    1. Add shoes app to Django installed apps (Complete)
    2. Create api urls (api_urls.py) (Complete)
    3. Create api views (api_views.py) (Complete)
    4. Create models (models.py) (Complete)
    shoes/poll
    1.ShoesForm & ShoesList components (Complete)
    2. Link components to App.js using Route (Complete)


I completed a functional react application that manages an inventory of shoes. I created two components, ShoeList and ShoeForm, that manages the data. The ShoeList displays a list of shoes that is fetched from the backend api. The details of the list displays each shoe with a picture. manufacturer name, model name, and color. Api requests were also created within the views to manage GET/POST/DELETE requests.


## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
