import React, { useState, useEffect } from 'react';

function ShoeForm() {

    const [formData, setFormData] = useState(
        {
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bins: '',
        }
    )

    const [bins, setBins] = useState([])

    const fetchBins = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchBins();
      }, []);


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/shoes/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bins: '',
            });
        }
    }


    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">

              <div className="form-floating mb-3">
                <input value={formData.manufacturer} onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="name">Manufacturer</label>
              </div>

              <div className="form-floating mb-3">
                <input value={formData.model_name} onChange={handleFormChange} placeholder="Model" required type="text" name="model_name" id="model_name" className="form-control" />
                <label htmlFor="starts">Model</label>
              </div>

              <div className="form-floating mb-3">
                <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="ends">Color</label>
              </div>

              <div className="form-floating mb-3">
                <input value={formData.picture_url} onChange={handleFormChange} placeholder="Picture" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="ends">Picture Url</label>
              </div>

              <div className="mb-3">
                <select value={formData.bins} onChange={handleFormChange} required name="bins" id="bins" className="form-select">
                  <option value="">Assign bin</option>
                  {bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Add shoe</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ShoeForm;