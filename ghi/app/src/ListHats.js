import React, {useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function ListHats(props) {
    const [hats, setHats] = useState([])


    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats"

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json()
                const requests = [];

                for (let hat of data.hats){
                    const detailURL = `http://localhost:8090${hat.href}`
                    requests.push(detailURL)
                }
                setHats(data.hats)

                console.log("Data:", data)
                console.log("Requests:", requests)
                
            }
        } catch (e) {
            console.error(e);
          }
    }

    useEffect(() => {
        fetchData();
      }, []);
    
    
    const handleDelete = async (hatHref) => {
        const url =  `http://localhost:8090${hatHref}`
        const fetchConfig = {
            method: "delete",
            body: JSON.stringify(hats),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok){
            console.log("Deleted successfully.")
            fetchData()
        }
        else {
            console.error(response)
        }
    };

    return (
        <div className="container py-5 text-center">
            <h1>The Hat Collection</h1>
            <div className="row">
                {hats.map((hat) => (
                    <div key={hat.id} className="col d-flex">
                        {console.log(hat.href)}
                        <div className='card my-3 p-3 rounded'>
                            <div className="img-container" style={{ height: "400px" }}>
                                <img className="img-fluid" src={hat.picture_url}/>
                            </div>
                            
                            <div className="card-body flex-fill">
                                <h5 className="card-title">{hat.style_name}</h5>
                                <p><strong>Material:</strong> {hat.fabric}</p>
                                <p><strong>Color:</strong> {hat.color}</p>
                                <p><strong>Location:</strong> {hat.location}</p>
                            </div>
                            <div className="mt-auto">
                            <Link to="" className="btn btn-primary btn-sm px-4 gap-3" onClick={() => handleDelete(hat.href)}>Delete</Link>
                            </div>
                        </div>
                    </div>
                    
                )

                )}

            </div>
            <div>
            <Link to="/hat/new" className="btn btn-primary btn-lg px-4 gap-3">Add hat</Link>
            </div>
        </div>
    );
  }

export default ListHats;