import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


function ShoeList(props) {
    const [shoes, setShoes] = useState([])


    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoes/"

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const requests = [];

                for (let shoe of data.shoes){
                    const detailURL = `http://localhost:8080${shoe.href}`
                    requests.push(detailURL)
                }
                setShoes(data.shoes)

                console.log("Data:" , data)
                console.log("Requests:", requests)

            }
        } catch (e) {
            console.error(e);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    const handleDelete = async (shoeHref) => {
        const url =  `http://localhost:8080${shoeHref}`
        const fetchConfig = {
            method: "delete",
            body: JSON.stringify(shoes),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            console.log("Deleted successfully.")
            fetchData()
        }
        else {
            console.error(response)
        }
    };

    return (
        <div className="container py-5 text-center">
            <h1>Sneakerheadz</h1>
            <div className="row">
                {shoes.map((shoe) => (
                    <div key={shoe.id} className="col d-flex">
                        {console.log(shoe.href)}
                        <div className='card my-3 p-3 rounded'>
                            <div className="img-container" style={{ height: "400px" }}>
                                <img className="img-fluid" src={shoe.picture_url}/>
                            </div>

                            <div className="card-body flex-fill">
                                <h5 className="card-title">{shoe.manufacturer_name}</h5>
                                <p><strong>Manufacturer:</strong> {shoe.manufacturer}</p>
                                <p><strong>Model:</strong> {shoe.model_name}</p>
                                <p><strong>Color:</strong> {shoe.color}</p>
                                <p><strong>Bin:</strong> {shoe.bins}</p>
                            </div>
                            <div className="mt-auto">
                            <Link to="" className="btn btn-primary btn-sm px-4 gap-3" onClick={() => handleDelete(shoe.href)}>Delete</Link>
                            </div>
                        </div>
                    </div>

                )

                )}

            </div>
            <div>
            <Link to="/shoe/new" className="btn btn-primary btn-lg px-4 gap-3">Add Shoe</Link>
            </div>
        </div>
    );
}

export default ShoeList;
