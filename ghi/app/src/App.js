import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Shoelist from './Shoelist';
import ShoeForm from './ShoeForm';
import ListHats from './ListHats';
import HatForm from './HatForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="/shoes" element={<Shoelist shoes={props.shoes} />} />
            <Route path="shoe/new" element={<ShoeForm />} />
            <Route path="/hats" element={<ListHats hats={props.hats} />} />
            <Route path="hat/new" element={<HatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
