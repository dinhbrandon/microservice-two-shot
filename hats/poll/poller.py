import django
import os
import sys
import time
import json
import requests
sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()
from hats_rest.models import LocationVO


def get_locations():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    locations = content["locations"]
    print("Test 1")
    for location in locations:
        print(location)
        print(location["closet_name"])
        print(location["section_number"])
        print(location["shelf_number"])
       
        LocationVO.objects.update_or_create(
            closet_name=location["closet_name"],
            section_number=location["section_number"],
            shelf_number=location["shelf_number"],
        )


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_locations()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
