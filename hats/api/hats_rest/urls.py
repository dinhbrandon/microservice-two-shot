from django.urls import path
from hats_rest.views import list_hats, show_hat_detail

urlpatterns = [
    path("hats/", list_hats, name="list_hats"),
    path("hats/<int:pk>/", show_hat_detail, name="show_hat_detail"),
]